const fs = require("fs");
const path = require("path");
const {isValidGenerator} = require("../utils/helpers");
const prompts = require("../prompts/index");
const actions = require("../actions/index");
const {generatorTypes} = require("../constants/params");

module.exports = plop => ({
  description: "Creates application structure",
  prompts: [
    {
      type: "list",
      name: "generator",
      message: "What do you want to generate?",
      default: generatorTypes.ALL,
      choices: [
        generatorTypes.ALL,
        generatorTypes.FRONTEND,
        generatorTypes.BACKEND,
        generatorTypes.DOCKER,
        generatorTypes.GRAPHQL,
      ],
    },
    ...prompts({plop}),
  ],
  actions: ({
    applicationPath,
    isModify,
    artifactId,
    packageName,
    groupId,
    generator,
    apiPath,
    webPath,
  }) => {
    // Check if application folder exists
    let appPath = applicationPath;

    const formattedPackageName = packageName || `${groupId}.${artifactId}`;
    if (!fs.existsSync(appPath)) {
      appPath = path.posix.join(process.cwd(), "{{dashCase applicationPath}}");
    }
    const defaultProps = {
      plop,
      appPath,
      renderProps: {
        applicationPath,
        packageName: formattedPackageName,
        webPath,
        apiPath,
      },
    };

    return [
      ...actions({
        defaultProps,
        isModify,
        data: {
          packageName: formattedPackageName,
          hasGraphQl: isValidGenerator(generator, [
            generatorTypes.GRAPHQL,
            generatorTypes.ALL,
          ]),
        },
        generator,
      }),
    ];
  },
});
