const {API_BASE_PATH, generatorTypes} = require("../../constants/params");
const {getActionObject, getFilePath} = require("../../utils/helpers");

/** BACKEND actions definition */
module.exports = ({defaultProps, isModify, data, generator}) =>
  [
    /** Config */
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{apiPath}}/config/checkstyle/checkstyle.xml",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${API_BASE_PATH}/config/checkstyle/checkstyle.xml`,
      // allowed generators
      generators: [generatorTypes.BACKEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{apiPath}}/config/findbugs/excludeFilter.xml",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${API_BASE_PATH}/config/findbugs/excludeFilter.xml`,
      generators: [generatorTypes.BACKEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    /** Application */
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/Application.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/Application.java.hbs`,
      generators: [generatorTypes.BACKEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/resources/application.properties",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${API_BASE_PATH}/src/main/resources/application.properties`,
      generators: [generatorTypes.BACKEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    /** Build */
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{apiPath}}/build.gradle",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/build.gradle.hbs`,
      generators: [
        generatorTypes.BACKEND,
        generatorTypes.ALL,
        generatorTypes.GRAPHQL,
      ],
      pattern: /[\s\S]*/,
    },
    /** GraphQl */
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/dao/AuthorDao.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/dao/AuthorDao.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/dao/PostDao.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/dao/PostDao.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/model/Author.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/model/Author.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/model/Post.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/model/Post.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/resolver/AuthorResolver.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/resolver/AuthorResolver.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/resolver/Mutation.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/resolver/Mutation.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/resolver/PostResolver.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/resolver/PostResolver.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/resolver/Query.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/resolver/Query.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/java/{{pathCase packageName}}/GraphQLConfiguration.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/java/cz/sbspa/GraphQLConfiguration.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{apiPath}}/src/main/resources/graphql/blog.graphqls",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/main/resources/graphql/blog.graphqls`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{apiPath}}/src/test/java/dao/AuthorTest.java",
          ...defaultProps,
        }),
        {isModify, data},
      ),
      templateFile: `${API_BASE_PATH}/src/test/java/dao/AuthorTest.java.hbs`,
      generators: [generatorTypes.GRAPHQL, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{apiPath}}/dockerfile",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${API_BASE_PATH}/dockerfile`,
      generators: defaultProps.renderProps.apiPath
        ? [generatorTypes.DOCKER, generatorTypes.ALL]
        : [],
      pattern: /[\s\S]*/,
    },
  ].filter(action => action.generators.includes(generator));
