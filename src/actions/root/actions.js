const {BASE_PATH, generatorTypes} = require("../../constants/params");
const {getActionObject, getFilePath} = require("../../utils/helpers");

/** ROOT actions definition */
module.exports = ({defaultProps, isModify, data, generator}) =>
  [
    /** Root configuration */
    {
      ...getActionObject(
        getFilePath({relativeFilePath: "settings.gradle", ...defaultProps}),
        {isModify, data},
      ),
      templateFile: `${BASE_PATH}/settings.gradle.hbs`,
      generators: [
        generatorTypes.GRAPHQL,
        generatorTypes.ALL,
        generatorTypes.FRONTEND,
        generatorTypes.BACKEND,
      ],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({relativeFilePath: "build.gradle", ...defaultProps}),
        {isModify, data},
      ),
      templateFile: `${BASE_PATH}/build.gradle`,
      generators: [
        generatorTypes.GRAPHQL,
        generatorTypes.ALL,
        generatorTypes.FRONTEND,
        generatorTypes.BACKEND,
      ],
      pattern: /[\s\S]*/,
    },
  ].filter(action => action.generators.includes(generator));
