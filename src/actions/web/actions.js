const {WEB_BASE_PATH, generatorTypes} = require("../../constants/params");
const {getActionObject, getFilePath} = require("../../utils/helpers");

/** FRONTEND actions definition */
module.exports = ({defaultProps, isModify, generator}) =>
  [
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/src/index.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/index.tsx.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/src/registerServiceWorker.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/registerServiceWorker.js`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    /** Configuration files */
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/public/assets/manifest.json",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/public/assets/manifest.json.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/public/template.html",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/public/template.html.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/.babelrc.js",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/.babelrc.js`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/.prettierignore",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/.prettierignore`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/.prettierrc",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/.prettierrc`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/jest.config.js",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/jest.config.js`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/jest.setup.ts",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/jest.setup.ts`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/package.json",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/package.json.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/tsconfig.json",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/tsconfig.json`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/tslint.json",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/tslint.json`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/webpack.config.js",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/webpack.config.js`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    /** Source files */
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/src/common/config/mockImage.js",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/common/config/mockImage.js`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/src/common/store/helpers.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/common/store/helpers.tsx.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/src/common/store/reducerRegistry.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/common/store/reducerRegistry.tsx.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/src/common/store/sagas.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/common/store/sagas.tsx.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/src/common/store/store.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/common/store/store.tsx.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/build.gradle",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/build.gradle`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/src/common/types/declarations.d.ts",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/common/types/declarations.d.ts`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{webPath}}/src/modules/{{pascalCase applicationPath}}.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/modules/App.tsx.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{webPath}}/src/modules/{{pascalCase applicationPath}}.test.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/modules/App.test.tsx.hbs`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{webPath}}/src/modules/shared/components/ReplaceMeComponent.scss",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/modules/shared/components/ReplaceMeComponent.scss`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{webPath}}/src/modules/shared/components/ReplaceMeComponent.test.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/modules/shared/components/ReplaceMeComponent.test.tsx`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{webPath}}/src/modules/shared/components/ReplaceMeComponent.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/modules/shared/components/ReplaceMeComponent.tsx`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{webPath}}/src/modules/shared/components/Router.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/modules/shared/components/Router.tsx`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath: "{{webPath}}/dockerfile",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/dockerfile`,
      generators: defaultProps.renderProps.webPath
        ? [generatorTypes.DOCKER, generatorTypes.ALL]
        : [],
      pattern: /[\s\S]*/,
    },
    {
      ...getActionObject(
        getFilePath({
          relativeFilePath:
            "{{webPath}}/src/modules/shared/components/Router.test.tsx",
          ...defaultProps,
        }),
        {isModify},
      ),
      templateFile: `${WEB_BASE_PATH}/src/modules/shared/components/Router.test.tsx`,
      generators: [generatorTypes.FRONTEND, generatorTypes.ALL],
      pattern: /[\s\S]*/,
    },
  ].filter(action => action.generators.includes(generator));
