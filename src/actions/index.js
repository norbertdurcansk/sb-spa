const webActions = require("./web/actions");
const apiActions = require("./api/actions");
const rootActions = require("./root/actions");

module.exports = props => [
  ...webActions(props),
  ...apiActions(props),
  ...rootActions(props),
];
