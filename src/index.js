const appGenerator = require("./generators/AppGenerator");

module.exports = plop => {
  plop.setGenerator("Application structure", appGenerator(plop));
};
