const fs = require("fs");
const path = require("path");
const {getFilePath} = require("../utils/helpers");
const {isValidGenerator} = require("../utils/helpers");
const {generatorTypes} = require("../constants/params");

module.exports = ({plop}) => {
  const resolvePath = ({applicationPath, apiPath}) => {
    let appPath = applicationPath;
    if (!fs.existsSync(appPath)) {
      appPath = path.posix.join(process.cwd(), "{{dashCase applicationPath}}");
    }

    return getFilePath({
      relativeFilePath: "{{pathCase apiPath}}",
      plop,
      renderProps: {
        applicationPath,
        apiPath,
      },
      appPath,
    });
  };

  return [
    {
      type: "input",
      name: "applicationPath",
      message: "Path to application folder / Application name:",
      validate: value => {
        if (/.+/.test(value)) {
          return true;
        }

        return "The application folder name is required";
      },
      when: ({generator}) =>
        isValidGenerator(generator, [
          generatorTypes.ALL,
          generatorTypes.FRONTEND,
          generatorTypes.BACKEND,
          generatorTypes.GRAPHQL,
          generatorTypes.DOCKER,
        ]),
    },
    {
      type: "confirm",
      name: "hasWebDocker",
      message: "Do you want dockerfile for Frontend?",
      default: false,
      when: ({generator}) =>
        isValidGenerator(generator, [generatorTypes.DOCKER]),
    },
    {
      type: "confirm",
      name: "hasApiDocker",
      message: "Do you want dockerfile for Backend?",
      default: false,
      when: ({generator}) =>
        isValidGenerator(generator, [generatorTypes.DOCKER]),
    },
    {
      type: "input",
      name: "apiPath",
      message: "Api directory path:",
      default: "api",
      validate: value => {
        if (/[a-zA-Z]+/.test(value)) {
          return true;
        }

        return "The api directory is required";
      },
      when: ({generator, hasApiDocker}) =>
        isValidGenerator(generator, [
          generatorTypes.ALL,
          generatorTypes.BACKEND,
          generatorTypes.GRAPHQL,
        ]) || hasApiDocker,
    },
    {
      type: "input",
      name: "webPath",
      message: "Web directory path:",
      default: "web",
      validate: value => {
        if (/[a-zA-Z]+/.test(value)) {
          return true;
        }

        return "The web directory is required";
      },
      when: ({generator, hasWebDocker}) =>
        isValidGenerator(generator, [
          generatorTypes.ALL,
          generatorTypes.FRONTEND,
        ]) || hasWebDocker,
    },
    {
      type: "input",
      name: "groupId",
      message: "GroupId:",
      description: "cz",
      default: "cz",
      validate: value => {
        if (/.+/.test(value)) {
          return true;
        }

        return "The groupId is required";
      },
      when: ({applicationPath, generator, apiPath}) =>
        !fs.existsSync(resolvePath({applicationPath, apiPath})) &&
        isValidGenerator(generator, [
          generatorTypes.ALL,
          generatorTypes.BACKEND,
          generatorTypes.GRAPHQL,
        ]),
    },
    {
      type: "input",
      name: "artifactId",
      message: "ArtifactId:",
      description: "sbspa",
      default: "sbspa",
      validate: value => {
        if (/.+/.test(value)) {
          return true;
        }

        return "The artifactId is required";
      },
      when: ({applicationPath, generator, apiPath}) =>
        !fs.existsSync(resolvePath({applicationPath, apiPath})) &&
        isValidGenerator(generator, [
          generatorTypes.ALL,
          generatorTypes.BACKEND,
          generatorTypes.GRAPHQL,
        ]),
    },
    {
      type: "input",
      name: "packageName",
      message: "Package name:",
      description: "cz.sbspa",
      default: "cz.sbspa",
      validate: value => {
        if (/.+/.test(value)) {
          return true;
        }

        return "The packageName is required";
      },
      when: ({applicationPath, generator, apiPath}) =>
        fs.existsSync(resolvePath({applicationPath, apiPath})) &&
        isValidGenerator(generator, [
          generatorTypes.ALL,
          generatorTypes.BACKEND,
          generatorTypes.GRAPHQL,
        ]),
    },
    {
      type: "confirm",
      name: "isModify",
      message: "Do you want to modify files (Commit before!!)?",
      default: false,
      when: ({generator}) =>
        isValidGenerator(generator, [
          generatorTypes.ALL,
          generatorTypes.FRONTEND,
          generatorTypes.BACKEND,
          generatorTypes.GRAPHQL,
          generatorTypes.DOCKER,
        ]),
    },
  ];
};
