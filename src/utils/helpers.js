const path = require("path");
const fs = require("fs");

module.exports = {
  getActionObject: (filePath, {isModify, data = {}}) => {
    if (isModify) {
      if (fs.existsSync(filePath)) {
        return {type: "modify", path: filePath, data};
      }
    }

    return {type: "add", path: filePath, data};
  },
  getFilePath: ({plop, appPath, relativeFilePath, renderProps}) =>
    plop.renderString(
      path.posix.join(path.resolve(appPath), relativeFilePath),
      renderProps,
    ),

  isValidGenerator: (type, validArray) => validArray.includes(type),
};
