module.exports = {
  BASE_PATH: "../templates",
  WEB_BASE_PATH: "../templates/web",
  API_BASE_PATH: "../templates/api",

  generatorTypes: {
    FRONTEND: "Frontend",
    BACKEND: "Backend",
    GRAPHQL: "GraphQL configuration",
    DOCKER: "Docker configuration",
    ALL: "All",
  },
};
