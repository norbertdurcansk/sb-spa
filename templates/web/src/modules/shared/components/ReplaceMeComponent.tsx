import * as React from "react";
import styles from "./ReplaceMeComponent.scss";

export default (props: any) => (
  <div className={styles.container}>
    <h3> This is temporary component (replace with actual component).</h3>
    <h4>Props:</h4>
    <code>
      {JSON.stringify(props)},<br />
    </code>
  </div>
);
