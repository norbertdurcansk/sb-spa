import {shallow} from "enzyme";
import * as React from "react";
import ReplaceMeComponent from "./ReplaceMeComponent";

describe("<ReplaceMeComponent/>", () => {
  const props = {
    prop1: "1",
    prop2: "2",
  };

  test("default", () => {
    const wrapper = shallow(<ReplaceMeComponent />);
    expect(wrapper).toMatchInlineSnapshot(`
<div>
  <h3>
     This is temporary component (replace with actual component).
  </h3>
  <h4>
    Props:
  </h4>
  <code>
    {}
    ,
    <br />
  </code>
</div>
`);
  });
  test("with props", () => {
    const wrapper = shallow(<ReplaceMeComponent {...props} />);
    expect(wrapper).toMatchInlineSnapshot(`
<div>
  <h3>
     This is temporary component (replace with actual component).
  </h3>
  <h4>
    Props:
  </h4>
  <code>
    {"prop1":"1","prop2":"2"}
    ,
    <br />
  </code>
</div>
`);
  });
});
